# E-shop
This is a web application that allows users to brouse and buy goods from different stores online. It is built using the Django framework and SQLite database.

## Badges
[![MIT License](https://img.shields.io/badge/License-MIT-%20orange.svg)](https://choosealicense.com/licenses/mit/)
![Python Version](https://img.shields.io/badge/Python-3%2B-blue)
![Project version](https://img.shields.io/badge/Version-1.11-dark%20green)


## Features

- A catalog of products with categories, prices, images, and descriptions
- A shopping cart that allows users to add, remove, and update items
- A checkout process 
- A user account system that enables users to register, login, logout, and view their order history
- A responsive design that adapts to different screen sizes and devices


## Tech stack

- Django
- MySQL
- Gitlab



## Installation

To install and run this project locally, you need to have Python 3.6 or higher and pip installed on your machine. Then follow these steps:

1. Clone this repository to your local machine with `git clone https://gitlab.com/group_2731205/e-shop.git`
2. Create and activate a virtual environment.
3. Install the required dependencies with `pip install -r requirements.txt`.
4. Set up the database with `python manage.py migrate`.
5. Create a superuser with `python manage.py createsuperuser`.
6. Run the development server with `python manage.py runserver`.
7. Visit `http://127.0.0.1:8000` to see the website.


    
## Usage

- Browse the product catalog and add items to your cart
- Sign in or register as a customer to complete the checkout process
- Sign in as an admin (username: admin, password: Admin123!) to access the dashboard and manage - products and orders
- Click on the item to purchase



## Contributers

 - [Stacy Kukunda](https://gitlab.com/stacykukunda)
 - [Otim Brian](https://gitlab.com/BrianOtim)
 - [Kesanyu Aisha](https://gitlab.com/nabahinda1234)
 - [Edward Ggayi](https://gitlab.com/Ed_09)


## Acknowledgements

 - [Django documentation](https://docs.djangoproject.com/en/5.0/)
 - [Gitlab documentation](https://docs.gitlab.com/)
 - [Python Django tutorial for beginners](https://www.youtube.com/watch?v=rHux0gMZ3Eg&t=572s&pp=ygUPZGphbmdvIHR1dG9yaWFs)
 - [MySQL tutorial for beginners](https://youtu.be/7S_tz1z_5bA)


## Technical and functional Specifications
## Technical Specification: E-Shop App (Built with Django)
1. Overview:
The E-Shop App is a Django-based online shopping application designed to provide users with a seamless and secure e-commerce experience. The technical specifications outline the Django-specific architecture, technologies, and functionalities of the system.

2. Architecture:
Django Framework:
Utilizes the Django web framework for rapid development and clean, maintainable code.
Follows the Model-View-Controller (MVC) architectural pattern.
Database:
Uses Django's Object-Relational Mapping (ORM) to interact with MySql.
Models for User, Product, Order, and other entities.
Frontend Integration:
Utilizes Django templates for rendering dynamic content.
May incorporate JavaScript (e.g., with Django REST framework) for enhanced user interactivity.
RESTful API:
Implements Django REST framework for building APIs.
Supports AJAX and asynchronous interactions.
3. Security:
Django Authentication:
Utilizes Django's built-in authentication system for user registration and login.
Implements user sessions and secure cookies.
Data Encryption:
Utilizes Django's security middleware for protection against common web vulnerabilities.
Implements encryption for sensitive data, such as passwords.
4. Scalability:
Django Channels:
Utilizes Django Channels for handling WebSockets and asynchronous operations.
Supports real-time updates and notifications.
Caching:
Integrates Django caching mechanisms to enhance performance.
Utilizes caching for frequently accessed data.
5. Testing:
Django Testing Framework:
Utilizes Django's testing framework for unit and integration testing.
May use third-party libraries like pytest for additional testing capabilities.
Automated Testing:
Implements automated testing for critical functionalities, views, and models.
6. Deployment:
Django Deployment Best Practices:
Utilizes WSGI servers like Gunicorn for deploying Django applications.
May deploy on popular platforms like Heroku, AWS, or Azure,Render.
Continuous Integration/Continuous Deployment (CI/CD):
**Implements CI/CD pipelines using tools like GitLab CI or Jenkins.
Automates testing and deployment processes.**
7. Django Admin:
Admin Panel Customization:
Customizes Django admin for efficient product and order management.
Provides administrative access for CRUD operations.
8. Django REST Framework:
API Endpoints:
Implements RESTful API endpoints for managing products, orders, and user accounts.
Supports CRUD operations through API.
Authentication and Authorization:
Utilizes Django REST framework's authentication classes for secure API access.
Implements token-based authentication.
9. Django Middleware:
Security Middleware:
Implements Django security middleware for protection against common security threats.
Configures middleware for secure HTTP headers.
10. Third-Party Integrations:
Payment Gateway Integration:
Integrates with third-party payment gateways compatible with Django (e.g., Django Oscar).
Ensures secure and reliable transactions.

## Functional Specification: E-Shop App (Built with Django)
The functional specification remains largely consistent with the previous outline. However, certain aspects are Django-specific:

1. User Registration and Authentication:
User Registration:
Leverages Django forms for user registration.
Utilizes Django signals for handling account verification.
Authentication:
Secure login using Django's built-in authentication views.
Password recovery and reset functionalities using Django's authentication system.
2. Product Catalog:
Product Listings:
Renders product catalog using Django templates.
Utilizes Django's QuerySets for efficient data retrieval.
Product Details:
Detailed product pages rendered with Django templates.
Utilizes Django's ORM for retrieving product information.
3. Shopping Cart:
Add to Cart:
Implements Django sessions for managing user carts.
Real-time updates on the cart contents using Django signals.
Checkout:
Secure checkout process with Django forms for user input.
Integrates with Django REST framework for API-driven checkout.
4. Order Management:
Order History:
Users can view their order history rendered with Django templates.
Order confirmation emails using Django's email sending capabilities.
5. User Account:
Profile Management:
Users can update personal information and password using Django forms.
Option to delete the account with Django views and signals.
6. Search and Navigation:
Search Functionality:
Efficient search functionality with Django's ORM and search filters.
Autocomplete suggestions for search queries using AJAX.
7. Notifications:
Email Notifications:
Sends email notifications using Django's email sending capabilities.
Configures email templates for various notifications.
8. Admin Panel:
Product Management:

Admins can add, edit, and delete products using Django admin.
Inventory management through Django admin.
Order Processing:

Admins can view and manage customer orders via Django admin.
Handle refunds and cancellations through Django admin.
This Django-specific technical and functional specification serves as a detailed guide for the development and implementation of the E-Shop App, ensuring adherence to Django best practices and principles.

## License

This project is licensed under the MIT License. See the [MIT LICENSE](https://gitlab.com/group_2731205/e-commerce-application/-/blob/main/LICENSE?ref_type=heads) file for details.
