"""
URL configuration for e_shop project.

This file defines the URL patterns for the e_shop project. URLs are mapped to views
which handle incoming requests and return the appropriate response.

For more information on URL routing in Django, please refer to the official documentation:
https://docs.djangoproject.com/en/5.0/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include  # Imports for URL routing
from . import settings  # Import project settings
from django.conf.urls.static import static  # Function for serving static files


# Import views from the e_shop1 app
from e_shop1 import views

urlpatterns = [
    
    path('admin/', admin.site.urls),# Admin site URL
    path('', views.index, name="index"),# Main website index view
    path('', include('e_shop1.urls')),  # Include e_shop1 app URLs
    path('business-registration/', include('business_registration.urls', namespace="business")),# Include URLs from the business_registration app with a namespace
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)# Serve static files in development mode (remove for production)
