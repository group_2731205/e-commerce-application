"""
WSGI config for e_shop project.

This file defines the entry point for the Django web application. It sets up
the environment variables and retrieves the WSGI callable for handling web requests.

For more information on WSGI configuration in Django, refer to the official documentation:
https://docs.djangoproject.com/en/5.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

# Set the environment variable for the Django settings module
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'e_shop.settings')

# Get the WSGI application callable
application = get_wsgi_application()

