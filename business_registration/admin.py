from django.contrib import admin

# Register your models here.
from.models import Business,  Category
admin.site.register(Business)
admin.site.register(Category)