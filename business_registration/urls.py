# business_registration/urls.py
from django.urls import path
from . import views
app_name = "business"
urlpatterns = [
    path('register/', views.register_business, name='register_business'),
    path('businesses/', views.business_list, name='business_list'),
    path('business/<int:business_id>/', views.business_detail, name='business_detail'),
    path('productregistration/', views.registerproduct, name='registerproduct'),
]
