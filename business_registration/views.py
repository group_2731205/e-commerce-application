from django.shortcuts import render, redirect, get_object_or_404
from .forms import BusinessRegistrationForm, ProductForm
from .models import Business
from django.contrib import messages





def register_business(request):
    if request.method == 'POST':
        form = BusinessRegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            business = form.save(commit=False)
            business.owner = request.user
            business.save()
            messages.success(request, ("You have registered successfully! Welcome to E-shop!"))
            return redirect('business:business_detail',business.id)
        else:
            messages.success(request, ("Whoops!, There was a problem Registering!, please try again."))
            return redirect('business_registration:register')
    else:
        form = BusinessRegistrationForm()
        return render(request, 'business_registration/register.html', {'form': form})


def business_list(request):
    businesses = Business.objects.all()
    return render(request, 'business_registration/businesses.html', {'businesses': businesses})


def registerproduct(request):

  if request.method == 'POST':
    form = ProductForm(request.POST, request.FILES)
    if form.is_valid():
      product = form.save(commit=False)
      product.save()
      
      return business_detail(request, product.business.id)

  else:
    form = ProductForm()

  return render(request, 'business_registration/productregister.html', {'form': form})

def business_detail(request, business_id):
  business = get_object_or_404(Business, id=business_id)
  products = business.products.filter(business=business)

  if business.owner == request.user:
        show_edit_and_add_product = True
  else:
        show_edit_and_add_product = False

  context = {
        'business': business,
        'products' :products,
        'show_edit_and_add_product': show_edit_and_add_product,
      }
  return render(request, 'business_registration/business_detail.html', context)








