from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True)
    catimage = models.ImageField(upload_to='photos/', default=None)
    class Meta:
        verbose_name_plural = 'Categories'
    def __str__(self):
        return self.name
class Business(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField()
    contact = models.CharField(max_length=11, blank=True)
    location = models.CharField(max_length=180)
    business_image = models.ImageField(upload_to='photos/', default=None ,unique=True, blank =True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, related_name='businesses')
    description = models.TextField(max_length=1000,blank=True)

    class Meta:
        verbose_name_plural = 'Registered Businesses'
    def __str__(self):
        return self.name




