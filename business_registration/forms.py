# registration/forms.py

from django import forms
from .models import Business
from e_shop1.models import Product

class BusinessRegistrationForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ['name',  'email', 'contact', 'location', 'business_image', 'category', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Business Name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Business Description'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
            'contact': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Contact Number'}),
            'location': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Business Location'}),
            'business_image': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'category': forms.Select(choices=(), attrs={'class': 'form-control'}),
        }



class ProductForm(forms.ModelForm):
  class Meta:
    model = Product
    fields = ['name', 'business', 'description', 'price', 'category', 'prodimage']
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Product Name'}),
        'business': forms.Select(choices=(), attrs={'class': 'form-control'}),
        'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Product Description'}),
        'price': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Price'}),
        'category': forms.Select(choices=(), attrs={'class': 'form-control'}),
        'prodimage': forms.ClearableFileInput(attrs={'class': 'form-control'}),

    }
