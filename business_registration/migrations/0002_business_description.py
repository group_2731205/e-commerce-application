# Generated by Django 4.2.11 on 2024-04-30 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business_registration', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='business',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
