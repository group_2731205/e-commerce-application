from django.test import TestCase
from django.contrib.auth.forms import UserCreationForm
from e_shop1.forms import SignUpForm
from django.test import TestCase
from django.contrib.auth.forms import UserCreationForm
from e_shop1.forms import SignUpForm
from business_registration.forms import BusinessRegistrationForm

class TestForms(TestCase):

    def test_signup_form_fields(self):
        form = SignUpForm()
        self.assertIsInstance(form, UserCreationForm)
        self.assertIn('username', form.fields)
        self.assertIn('password1', form.fields)
        self.assertIn('password2', form.fields)



class TestForms(TestCase):

    def test_signup_form_fields(self):
        form = SignUpForm()
        self.assertIsInstance(form, UserCreationForm)
        self.assertIn('username', form.fields)
        self.assertIn('password1', form.fields)
        self.assertIn('password2', form.fields)

    def test_business_registration_form_fields(self):
        form = BusinessRegistrationForm()
        self.assertIn('name', form.fields)
        self.assertIn('email', form.fields)
        self.assertIn('contact', form.fields)
        self.assertIn('location', form.fields)
        self.assertIn('business_image', form.fields)
        self.assertIn('category', form.fields)
        self.assertIn('description', form.fields)