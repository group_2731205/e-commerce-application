from django.test import SimpleTestCase
from django.urls import reverse, resolve
from e_shop1 import views
from business_registration import views as business_views

class UrlsTestCase(SimpleTestCase):
    def test_index_url(self):
        url = reverse('e_shop1:index')
        self.assertEqual(resolve(url).func, views.index)

    def test_singlecategory_url(self):
        url = reverse('e_shop1:category', args=['test-category'])
        self.assertEqual(resolve(url).func, views.singlecategory)

    def test_categories_url(self):
        url = reverse('e_shop1:categories')
        self.assertEqual(resolve(url).func, views.categories)

    def test_search_url(self):
        url = reverse('e_shop1:search')
        self.assertEqual(resolve(url).func, views.search)

    def test_businesses_url(self):
        url = reverse('e_shop1:businesses')
        self.assertEqual(resolve(url).func, views.businesses)

    def test_about_url(self):
        url = reverse('e_shop1:about')
        self.assertEqual(resolve(url).func, views.about)

    def test_products_url(self):
        url = reverse('e_shop1:products')
        self.assertEqual(resolve(url).func, views.products)

    def test_login_url(self):
        url = reverse('e_shop1:login')
        self.assertEqual(resolve(url).func, views.login_user)

    def test_logout_url(self):
        url = reverse('e_shop1:logout')
        self.assertEqual(resolve(url).func, views.logout_user)

    def test_register_url(self):
        url = reverse('e_shop1:register')
        self.assertEqual(resolve(url).func, views.register_user)

    def test_productdetail_url(self):
        url = reverse('e_shop1:productdetail', args=['test-product-id'])
        self.assertEqual(resolve(url).func, views.productdetail)

    def test_checkout_url(self):
        url = reverse('e_shop1:checkout')
        self.assertEqual(resolve(url).func, views.checkout)

    def test_cart_url(self):
        url = reverse('e_shop1:cart')
        self.assertEqual(resolve(url).func, views.cart)

    def test_cart_update_url(self):
        url = reverse('e_shop1:cart_update')
        self.assertEqual(resolve(url).func, views.cart_update)

    def test_postReview_url(self):
        url = reverse('e_shop1:add review', args=[1])
        self.assertEqual(resolve(url).func, views.postReview)

    def test_postRating_url(self):
        url = reverse('e_shop1:add rating', args=[1])
        self.assertEqual(resolve(url).func, views.postRating)

    def test_register_business_url(self):
        url = reverse('business:register_business')
        self.assertEqual(resolve(url).func, business_views.register_business)

    def test_business_list_url(self):
        url = reverse('business:business_list')
        self.assertEqual(resolve(url).func, business_views.business_list)

    def test_business_detail_url(self):
        url = reverse('business:business_detail', args=[1])
        self.assertEqual(resolve(url).func, business_views.business_detail)

    def test_registerproduct_url(self):
        url = reverse('business:registerproduct')
        self.assertEqual(resolve(url).func, business_views.registerproduct)
