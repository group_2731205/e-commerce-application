from django.db import models
from django.contrib.auth.models import User
from business_registration.models import Business, Category

# Model for Products
class Product(models.Model):
    name = models.CharField(max_length=255)  # Product name
    description = models.TextField(blank=True)  # Product description (optional)
    price = models.DecimalField(max_digits=10, decimal_places=2)  # Product price
    category = models.ForeignKey(Category, on_delete=models.CASCADE)  # Foreign key to Category model
    stock_quantity = models.PositiveIntegerField(default=0)  # Number of items in stock
    prodimage = models.ImageField(upload_to='photos/', default=None)  # Product image upload field
    business = models.ForeignKey(Business, related_name='products', on_delete=models.CASCADE, blank=True, null=True)  # Foreign key to Business model (optional)

    def __str__(self):
        return self.name  # Defines how the product object is represented as a string

# Model for Customers
class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)  # One-to-one relationship with User model
    address = models.TextField(blank=True, null=True)  # Customer's address (optional)
    phone_number = models.CharField(max_length=15, blank=True, null=True)  # Customer's phone number (optional)

    def __str__(self):
        return self.user.username  # Defines how the customer object is represented as a string

    @property
    def sum_cart(self):
        orders = self.order_set.all()  # Get all orders associated with the customer
        item_number = sum([x.get_cart_items for x in orders])  # Sum the quantity of items from all orders
        return item_number  # Return the total number of items in the cart

# Model for Orders
class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)  # Foreign key to Customer model (can be null)
    # date_ordered = models.DateTimeField(auto_now_add=True)  # Commented out, might not be needed
    complete = models.BooleanField(default=False, null=True, blank=True)  # Indicates if the order is completed
    transaction_id = models.CharField(max_length=200, null=True)  # Transaction ID (optional)

    def __str__(self):
        return str(self.id)  # Defines how the order object is represented as a string

    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()  # Get all order items associated with the order
        total = sum([item.get_total for item in orderitems])  # Sum the total price of all order items
        return total  # Return the total price of the order

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()  # Get all order items associated with the order
        total = sum([item.quantity for item in orderitems])  # Sum the quantity of all order items
        return total  # Return the total number of items in the order

# Model for Order Items (linking Orders and Products)
class OrderItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, blank=True, null=True)  # Foreign key to Product model (can be null)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)  # Foreign key to Order model (can be null)
    quantity = models.PositiveIntegerField(default=0, null=True, blank=True)  # Quantity of the product in the order
    date_added = models.DateTimeField(auto_now_add=True)  # Date and time the item was added to the order

    @property
    def get_total(self):
        total = self.product.price * self.quantity  # Calculate the total price for this order item
        return total  # Return the total price

# Checkout model: Represents a checkout process for an order
class Checkout(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)  # One-to-one relationship with User model
    order = models.ForeignKey(Order, on_delete=models.CASCADE)  # Foreign key relationship with Order model
    payment_method = models.CharField(max_length=100)  # Payment method (e.g., credit card, PayPal)
    address = models.TextField()  # Shipping address
    phone_number = models.CharField(max_length=15)  # Contact phone number
    additional_notes = models.TextField(blank=True)  # Optional additional notes
    checkout_date = models.DateTimeField(auto_now_add=True)  # Timestamp when checkout occurred

    def __str__(self):
        return f"Checkout #{self.id} by {self.user}"  # Human-readable representation of the checkout

# Review model: Represents a product review by a user
class Review(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='reviews')  # Product associated with the review
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)  # User who wrote the review
    text = models.TextField()  # Review text
    created_at = models.DateTimeField(auto_now_add=True)  # Timestamp when the review was created

# Rating model: Represents a user's rating for a product
class Rating(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='ratings')  # Product associated with the rating
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)  # User who provided the rating
    value = models.IntegerField()  # Numeric value representing the rating (e.g., 1 to 5 stars)

    @property
    def get_value(self):
        return self.value  # Property method to retrieve the rating value

