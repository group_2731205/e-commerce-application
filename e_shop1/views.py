from django.shortcuts import render, redirect
from django.db.models import Q
from .models import *
from django.contrib.auth import authenticate, login, logout 
from django.contrib import messages
from .forms import SignUpForm
import json, math
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from business_registration.models import Business
from e_shop1.models import Product
import math

# Create your views here.
def index(request):
    """View function for the home page."""
    products = Product.objects.all()
    categories = Category.objects.all()
    return render(request, 'e_shop1/index.html', {"products": products, "categories": categories})
    


def businesses(request):
    businesses = Business.objects.all()
    return render(request, 'e_shop1/businesses.html', {'businesses': businesses})


def products(request):
    """Show all products"""
    products = Product.objects.all()
    return render(request, 'e_shop1/product.html', { 'products' : products})


def categories(request):
    """Show all categories"""
    categories = Category.objects.all()
    return render(request, 'e_shop1/categories.html', {'categories': categories})

def singlecategory(request, category_id):
    """Show single category"""
    category = Category.objects.get(id = category_id)
    products = Product.objects.filter(category=category)
    return render(request, 'e_shop1/singlecategory.html', {'categories': categories,'products':products})


def about(request):
    return render(request, 'e_shop1/about.html', {})

def login_user(request):
   if request.method == "POST":
       username =request.POST.get('username')
       password = request.POST.get('password')
       user = authenticate(request , username=username, password=password)
       if user is not None:
           login(request, user)
           messages.success(request, ("Logged in successfully! Welcome to E-Shop"))
           return redirect('e_shop1:index')
       else:
           messages.warning(request, ('Username or Password is incorrect'))
           return redirect('e_shop1:login')
   else:
        return render(request, 'e_shop1/login.html', {}) 

def logout_user(request):
    logout(request)
    messages.success(request, ("Logged out successfully!"))
    return redirect('e_shop1:index') 

def register_user(request):
    form = SignUpForm()
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, ("You have registered successfully! Welcome to E-shop!"))
            return redirect('e_shop1:index')
        
        else:
            messages.success(request, ("Whoops!, There was a problem Registering!, please try again."))
            return redirect('e_shop1:register')

    else:    
        return render(request, 'e_shop1/register.html', {'form': form })

def productdetail(request,pk ):
    """View function for the home page."""
    products = Product.objects.get(id=pk)
    ratings =  Rating.objects.filter(product=products)
    reviews = Review.objects.filter(product=products)
   

    if len(ratings) !=0 :
        average_rating = sum([x.get_value for x in ratings])/len(ratings)
        average_rating = math.ceil(average_rating)
        rating_list = [i for i in range(average_rating)]

    else:
        average_rating =0
        rating_list=[ ]  
    
    return render(request, 'e_shop1/productdetail.html',{ 'product' : products, 'rating' : average_rating, 'rating_list' : rating_list, 'reviews' : reviews} )

def postReview(request, product_id):
    product = Product.objects.get(id=product_id)
    review = request.POST.get('review')
    new = Review.objects.create(
        product = product,
        user = request.user.customer,
        text = review
    )
    new.save()
    return redirect('e_shop1:productdetail', product_id)



def postRating(request, product_id):
    product = Product.objects.get(id=product_id)
    rating = request.POST.get('rate')
    new = Rating.objects.create(
        product = product,
        user = request.user.customer,
        value = rating
    )
    new.save()
    return redirect('e_shop1:productdetail', product_id)




def search(request):
    if request.method == 'GET':
        query = request.GET.get('query')

        if query:
            # Combined search across products, categories, and businesses
            results = Q(name__icontains=query)

            product_results = results |  Q(price__icontains=query)
            products = Product.objects.filter(product_results).distinct()
            

            # Search businesses by name, description, and location
            business_results = results | Q(location__icontains=query)
            businesses = Business.objects.filter(business_results).distinct()

            category_results = results |  Q(description__icontains=query)
            categories = Category.objects.filter(category_results).distinct()

            # Context dictionary with combined search results
            context = {
                'products': products,
                'categories': categories,
                'businesses': businesses,
            }

            return render(request, 'e_shop1/search.html', context)

        else:
            # Handle empty search query gracefully (optional)
            context = {'message': 'Please enter a search term.'}
            return render(request, 'e_shop1/search.html', context)

    else:
            print("Search could not be found")
            return request(request, 'e_shop1/search.html', {})
    



def cart(request):
    if request.user.is_authenticated:
        try:
            customer = request.user.customer
            order, created = Order.objects.get_or_create(customer=customer, complete=False)
            items = order.orderitem_set.all()
        except ObjectDoesNotExist:
            items = []
            order = {'get_cart_total': 0, 'get_cart_items': 0}
        context = {
            'items': items,
            'order': order,
        }
        return render(request, "e_shop1/cart1.html", context)
    else:
        messages.warning(request, 'You must login first!')
        return redirect('e_shop1:index')



def checkout(request):
    if request.user.is_authenticated:
        order, created = Order.objects.get_or_create(customer=request.user.customer, complete = False)
        items = order.orderitem_set.all()
    else:    
        items=[]
        order ={'get_cart_total':0,'get_cart_items':0}
    context = {'items':items, 
               'order':order}
    return render(request,"e_shop1/checkout.html", context)

def cart_update(request):  
    if request.user.is_authenticated:
        data = json.loads(request.body)
        productId = data['product_id']
        action = data['action']

        customer = Customer.objects.get(user=request.user)
        product = Product.objects.get(id=productId)
        order,created = Order.objects.get_or_create(customer=customer, complete=False)
        order_item,created = OrderItem.objects.get_or_create(order=order, product=product)
        if action == "add":

            order_item.quantity = (order_item.quantity+1)
            messages.success(request, 'Product added to cart')
        elif action == "remove":
            order_item.quantity = (order_item.quantity-1)
            messages.warning(request, 'Item removed from cart')

        order_item.save()

        if order_item.quantity <= 0:
            order_item.delete()

        return JsonResponse(action + ' complete', safe=False)
    else:
        messages.warning(request, 'You must login first!')
        return redirect('e_shop1:products')


    

        