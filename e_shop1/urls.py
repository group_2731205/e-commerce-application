# Defines URL patterns for the e_shop1 app.

from django.urls import path, include
from . import views

app_name = 'e_shop1'
urlpatterns = [
    # Home page
    path('', views.index, name='index'),

    # Category related URLs
    path('category/<str:category_id>', views.singlecategory, name='category'),
    path('categories/', views.categories, name='categories'),

    # Search functionality
    path('search/', views.search, name='search'),

    # Business listing
    path('businesses/', views.businesses, name='businesses'),

    # About page
    path('about/', views.about, name='about'),

    # Product listing
    path('product/', views.products, name='products'),

    # User authentication URLs
    path('login/', views.login_user, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('register/', views.register_user, name='register'),

    # Product details page with dynamic product ID
    path('productdetail/<str:pk>', views.productdetail, name='productdetail'),

    # Checkout page
    path('checkout/', views.checkout, name='checkout'),

    # Shopping cart functionalities
    path('cart/', views.cart, name='cart'),
    path('update/', views.cart_update,name='cart_update'),

    # Product review and rating URLs
    path('review/<int:product_id>',views.postReview, name= "add review"),
    path('rate/<int:product_id>',views.postRating, name= "add rating"),
]
